package exception;

import response.ServiceResult;

public class ResultException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    ServiceResult<Object> resultExecption;

    public ResultException(ServiceResult<Object> resultExecption) {
        super(resultExecption.getMessage());
        this.resultExecption = resultExecption;
    }

    public ServiceResult<Object> getResultExecption() {
        return resultExecption;
    }
}
