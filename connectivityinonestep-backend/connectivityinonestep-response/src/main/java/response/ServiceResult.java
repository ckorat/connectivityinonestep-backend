package response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ServiceResult<T> {
    private int code;
    private String message;
    private T result;
    private List<ConnectivityInOneStepError> errors;

    public ServiceResult() {
    }

    public ServiceResult(int code, String message) {
        super();
        this.code = code;
        this.message = message;
    }

    public ServiceResult(int code, T result) {
        this.code = code;
        this.result = result;
    }

    public ServiceResult(int code, String message, T result) {
        this.code = code;
        this.message = message;
        this.result = result;
    }

    public ServiceResult(int code, List<ConnectivityInOneStepError> error) {
        super();
        this.code = code;
        this.errors = error;
    }

    public ServiceResult(int code, String message, List<ConnectivityInOneStepError> error) {
        super();
        this.code = code;
        this.message = message;
        this.errors = error;
    }

    public ServiceResult(int code, ConnectivityInOneStepError error) {
        super();
        this.code = code;
        addErrorToList(error);
    }

    public ServiceResult(int code, String message, ConnectivityInOneStepError error) {
        super();
        this.code = code;
        this.message = message;
        addErrorToList(error);
    }

    public ServiceResult(int code, String message, T result, List<ConnectivityInOneStepError> error) {
        super();
        this.code = code;
        this.message = message;
        this.result = result;
        this.errors = error;
    }

    public void addErrorToList(ConnectivityInOneStepError error) {
        if (this.errors == null) {
            this.errors = new ArrayList<ConnectivityInOneStepError>();
        }
        this.errors.add(error);
    }

    @Override
    public String toString() {
        return "ConnectivityInOneStep [code="+code+",message=" + message + ", result=" + result + ",errors=" + errors + "]";
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class ConnectivityInOneStepError<T>{
        private int  code;
        private String message;
        private T result;

        public ConnectivityInOneStepError(int code, String message) {
            super();
            this.code = code;
            this.message = message;
        }

        public ConnectivityInOneStepError(int code, String message, T result) {
            super();
            this.code = code;
            this.message = message;
            this.result = result;
        }

        @Override
        public String toString() {
            return "ConnectivityInOneStepError [code="+code+",message=" + message + ", result=" + result + "]";
        }
    }

}
