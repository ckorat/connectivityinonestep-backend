package com.connectivityinonestep.admin.repository;

import com.connectivityinonestep.admin.model.RegisteredProvider;
import com.connectivityinonestep.admin.model.AdminPassword;
import com.connectivityinonestep.admin.model.UpdateRegistrationDetails;
import com.connectivityinonestep.admin.model.mapper.RegisteredProviderMapper;
import com.connectivityinonestep.admin.utils.ServiceProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SingleColumnRowMapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Random;

@Repository
public class AdminDao implements IAdminDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private ServiceProperties properties;

    @Override
    public String registerprovider(RegisteredProvider registeredProvider) {
        String password = generateRandomPassword(8);
        int affectedRows = jdbcTemplate.update(properties.getDbQueries().getRegisterProvider(),registeredProvider.getName(),
                            registeredProvider.getContactNumber(),registeredProvider.getEmail(),
                            password,registeredProvider.getLastLoginDate(),registeredProvider.getProviderId()
        );
        if(affectedRows>0)
        {
            return password;
        }
        return "";
    }

    @Override
    public List<RegisteredProvider> getAllRegisteredProviders() {
        return jdbcTemplate.query(properties.getDbQueries().getRegisteredProvidersList(),new RegisteredProviderMapper());
    }

    @Override
    public List<String> checkEmailExist(String email) {
        List<String> registeredEmail = jdbcTemplate.query(properties.getDbQueries().getCheckEmailExist(),new SingleColumnRowMapper<>(),email);
        return registeredEmail;
    }

    @Override
    public int updatePassword(AdminPassword adminPassword) {
        List<Integer> oldpasswordcheck = jdbcTemplate.query(properties.getDbQueries().getCheckOldPassword(),new SingleColumnRowMapper<>(),adminPassword.getOldPassword(),adminPassword.getAdminId());
        if(oldpasswordcheck.isEmpty())
        {
            return -1;
        }
        return jdbcTemplate.update(properties.getDbQueries().getUpdatePassword(),adminPassword.getNewPassword(),adminPassword.getAdminId());
    }

    @Override
    public int updateStatus(int registrationId, int status) {
        return jdbcTemplate.update(properties.getDbQueries().getUpdateRegisteredProviderStatus(),status,registrationId);
    }

    @Override
    public int updateRegistrationDetails(UpdateRegistrationDetails updateDetails) {
        return jdbcTemplate.update(properties.getDbQueries().getUpdateRegistrationDetails(),updateDetails.getName(),updateDetails.getContactNumber(),updateDetails.getRegistrationId());
    }

    public String generateRandomPassword(int length)
    {
        String characters = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        Random rnd = new Random();

        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            sb.append(characters.charAt(rnd.nextInt(characters.length())));
        }
        return sb.toString();
    }


}
