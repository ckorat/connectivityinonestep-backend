package com.connectivityinonestep.admin.controller;


import com.connectivityinonestep.admin.model.RegisteredProvider;
import com.connectivityinonestep.admin.model.AdminPassword;
import com.connectivityinonestep.admin.model.UpdateRegistrationDetails;
import com.connectivityinonestep.admin.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import response.ServiceResult;

import javax.mail.MessagingException;
import javax.validation.Valid;
import java.io.IOException;
import java.util.List;
import java.util.Map;

@CrossOrigin(origins ="*", allowedHeaders = "*")
@RestController
@RequestMapping("/admin")
public class AdminController {
    @Autowired
    AdminService adminService;


    @GetMapping("/getRegisteredProviders")
    public ResponseEntity<ServiceResult<List<RegisteredProvider>>> getAllRegisteredProviders()
    {
        ServiceResult result = new ServiceResult<>(200,adminService.getAllRegisteredProviders());
        return new ResponseEntity<>(result, HttpStatus.valueOf(result.getCode()));
    }

    @GetMapping("/checkEmailExists")
    public ResponseEntity<ServiceResult<Map<String,Boolean>>> checkEmailExist(
            @RequestParam("email") String email
    )
    {
        ServiceResult result = new ServiceResult<>(200,adminService.checkEmailExist(email));
        return new ResponseEntity<>(result, HttpStatus.valueOf(result.getCode()));
    }

    @PostMapping("/registered-provider")
    public ResponseEntity<ServiceResult<String>> registerProviders(@RequestBody @Valid RegisteredProvider registeredProvider) throws IOException, MessagingException {
        ServiceResult result = new ServiceResult<>(201,adminService.registerProvider(registeredProvider));
        return new ResponseEntity<>(result, HttpStatus.valueOf(result.getCode()));
    }

    @PutMapping("/changepassword")
    public ResponseEntity<ServiceResult<String>> updatePassword(@RequestBody @Valid AdminPassword adminPassword)
    {
        ServiceResult<String> result = adminService.updatePassword(adminPassword);
        return new ResponseEntity<>(result, HttpStatus.valueOf(result.getCode()));
    }

    @PutMapping("/changestatus/{registrationId}/{status}")
    public ResponseEntity<ServiceResult<String>> updateRegisteredProviderStatus(
            @PathVariable("registrationId") int registrationId,
            @PathVariable("status") int status
    )
    {
        ServiceResult<String> result = adminService.updateRegisteredProviderStatus(registrationId,status);
        return new ResponseEntity<>(result, HttpStatus.valueOf(result.getCode()));
    }

    @PutMapping("/updateProfile")
    public ResponseEntity<ServiceResult<String>> updateRegisterationDetails(
            @RequestBody @Valid UpdateRegistrationDetails updateDetails
            )
    {
        ServiceResult<String> result = adminService.updateRegisterationDetails(updateDetails);
        return new ResponseEntity<>(result, HttpStatus.valueOf(result.getCode()));
    }
}
