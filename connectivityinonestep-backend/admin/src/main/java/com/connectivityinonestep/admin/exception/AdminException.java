package com.connectivityinonestep.admin.exception;

import exception.ResultException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import response.ServiceResult;

import java.util.*;

@RestControllerAdvice
@Slf4j
public class AdminException extends ResponseEntityExceptionHandler {



    @ExceptionHandler(Exception.class)
    private ResponseEntity<Map<String, Object>> Exception(Exception ex) {
        Map<String, Object> map = new HashMap<>();
        map.put("code", ex.getMessage().hashCode());
        map.put("message", "Unfortunately! there was some error at the server side.");
        logger.error(ex.getCause());
        return new ResponseEntity<>(map, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler({ResultException.class})
    private ResponseEntity<ServiceResult<Object>> resultException(ResultException ex) {
        ServiceResult<Object> result = ex.getResultExecption();
        return new ResponseEntity<>(result, HttpStatus.valueOf(result.getCode()));
    }

    @Override
    protected ResponseEntity<Object> handleTypeMismatch(TypeMismatchException ex, HttpHeaders headers,
                                                        HttpStatus status, WebRequest request) {
        logger.error("\n" + ex.hashCode() + " ---- " + ex.getMessage());
        return new ResponseEntity<>(
                new ServiceResult<>(400, "Error! unable to parse the given data",
                        new ArrayList<>(
                                Arrays.asList(new ServiceResult.ConnectivityInOneStepError(ex.hashCode(), ex.getLocalizedMessage())))),
                HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers, HttpStatus status, WebRequest request) {
        logger.error("\n" + ex.hashCode() + " ---- " + ex.getMessage());
        List<String> details = new ArrayList<>();
        for(FieldError error : ex.getBindingResult().getFieldErrors()) {
            details.add(error.getField()+" "+error.getDefaultMessage());
        }
        return new ResponseEntity<>(new ServiceResult<>(400, "Missing! Object body missing",details), HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
                                                                  HttpHeaders headers, HttpStatus status, WebRequest request) {
        logger.error("\n" + ex.hashCode() + " ---- " + ex.getMessage());
        List<String> details = new ArrayList<>();
        details.add(ex.getLocalizedMessage());
        return new ResponseEntity<>(
                new ServiceResult<>(400, "Error! unable to parse the given data",details),
                HttpStatus.BAD_REQUEST);
    }



}
