package com.connectivityinonestep.admin.repository;

import com.connectivityinonestep.admin.model.RegisteredProvider;
import com.connectivityinonestep.admin.model.AdminPassword;
import com.connectivityinonestep.admin.model.UpdateRegistrationDetails;

import java.util.List;

public interface IAdminDao {

    String registerprovider(RegisteredProvider registeredProvider);
    List<RegisteredProvider> getAllRegisteredProviders();
    List<String> checkEmailExist(String email);
    int updatePassword(AdminPassword adminPassword);
    int updateStatus(int registrationId,int Status);
    int updateRegistrationDetails(UpdateRegistrationDetails updateDetails);
}
