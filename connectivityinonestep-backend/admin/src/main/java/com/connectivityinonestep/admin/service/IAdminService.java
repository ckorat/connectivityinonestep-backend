package com.connectivityinonestep.admin.service;

import com.connectivityinonestep.admin.model.AdminPassword;
import com.connectivityinonestep.admin.model.RegisteredProvider;
import com.connectivityinonestep.admin.model.UpdateRegistrationDetails;
import response.ServiceResult;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface IAdminService {
    ServiceResult<String> registerProvider(RegisteredProvider registeredProvider) throws IOException, MessagingException;
    List<RegisteredProvider> getAllRegisteredProviders();
    Map<String,Boolean> checkEmailExist(String email);
    ServiceResult<String> updatePassword(AdminPassword adminPassword);
    ServiceResult<String> updateRegisteredProviderStatus(int registrationId,int status);
    ServiceResult<String> updateRegisterationDetails(UpdateRegistrationDetails updateDetails);
}
