package com.connectivityinonestep.admin.client;

import com.connectivityinonestep.messagingservice.Model.Mail;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(url="http://localhost:8095/messaging",name="MailClient")
public interface MailFeignClient {

    @PostMapping("/sendmail")
    public void sendmail(@RequestBody Mail mail);
}
