package com.connectivityinonestep.admin.model.mapper;

import com.connectivityinonestep.admin.model.RegisteredProvider;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class RegisteredProviderMapper implements RowMapper<RegisteredProvider> {
    @Override
    public RegisteredProvider mapRow(ResultSet row, int rowNum) throws SQLException {
        RegisteredProvider registeredProvider = new RegisteredProvider();
        registeredProvider.setRegistrationId(row.getInt("registrationId"));
        registeredProvider.setName(row.getString("name"));
        registeredProvider.setContactNumber(row.getString("contactNumber"));
        registeredProvider.setEmail(row.getString("email"));
        registeredProvider.setProviderName(row.getString("providerName"));
        registeredProvider.setProviderId(row.getInt("providerId"));
        registeredProvider.setLastLoginDate(row.getDate("lastLoginDate"));
        registeredProvider.setStatus(row.getInt("status"));
//        if(row.getInt("status") == 0)
//        {
//            registeredProvider.setStatus("false");
//        }
//        else
//        {
//            registeredProvider.setStatus("true");
//        }
        return registeredProvider;
    }
}
