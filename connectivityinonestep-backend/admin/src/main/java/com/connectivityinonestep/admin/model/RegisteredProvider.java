package com.connectivityinonestep.admin.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.sql.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor

public class RegisteredProvider implements Serializable {

    private int registrationId;

    @NotNull
    private String name;

    @NotNull
    private String contactNumber;

    @NotNull
    private String email;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    private Date lastLoginDate;

    private String providerName;

    @NotNull
    private int providerId;

    private int status;
}
