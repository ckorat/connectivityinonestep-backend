package com.connectivityinonestep.admin.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
public class UpdateRegistrationDetails implements Serializable {

    private int registrationId;
    @NotNull
    private String name;
    @NotNull
    private String contactNumber;
}
