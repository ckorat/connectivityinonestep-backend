package com.connectivityinonestep.admin.utils;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ConfigurationProperties(prefix = "db")
@Component
public class ServiceProperties {

    @NotNull
    public DbQueries dbQueries;

    @Data
    @Component
    @AllArgsConstructor
    @NoArgsConstructor
    public static class DbQueries {
        private String registerProvider;
        private String registeredProvidersList;
        private String checkEmailExist;
        private String checkOldPassword;
        private String updatePassword;
        private String updateRegisteredProviderStatus;
        private String updateRegistrationDetails;
    }



}
