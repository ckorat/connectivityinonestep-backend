package com.connectivityinonestep.admin.service;

import com.connectivityinonestep.admin.client.MailFeignClient;
import com.connectivityinonestep.admin.model.AdminPassword;
import com.connectivityinonestep.admin.model.RegisteredProvider;
import com.connectivityinonestep.admin.model.UpdateRegistrationDetails;
import com.connectivityinonestep.admin.repository.AdminDao;
import com.connectivityinonestep.messagingservice.Model.Mail;
import exception.ResultException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import response.ServiceResult;


import javax.mail.MessagingException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AdminService implements IAdminService{
    @Autowired
    private AdminDao adminDao;

    @Autowired
    private MailFeignClient mailFeignClient;

    @Override
    public ServiceResult<String> registerProvider(RegisteredProvider registeredProvider) throws IOException, MessagingException {
        String password = adminDao.registerprovider(registeredProvider);
        if(password.isEmpty())
        {
            throw new ResultException(new ServiceResult<>(500, "We Are Facing Some Issue With Your Request. Please Try Again Later."));

        }
        String subject = "ConnectivityInOneStep Welcomes You!\n";
        String message = "Dear "+registeredProvider.getName()+"<br/>  You are now successfully registered with us.You can Login with the following credentials<br/> <br/> Email: " +
                        ""+registeredProvider.getEmail()+"<br/> Password: "+ password+"<br/><br/> It is Recommended to initially login with" +
                " this password and then you change your password <br/> Thank you!!";
        Mail mail = new Mail();
        mail.setTo(registeredProvider.getEmail());
        mail.setName(registeredProvider.getName());
        mail.setContent(message);
        mail.setSubject(subject);
        mailFeignClient.sendmail(mail);
        return new ServiceResult<>(200,"Updated Successfully");
    }

    @Override
    public List<RegisteredProvider> getAllRegisteredProviders() {
        List<RegisteredProvider> registeredProviders = adminDao.getAllRegisteredProviders();
        if(registeredProviders.isEmpty())
        {
            throw new ResultException(new ServiceResult<>(500, "We Are Facing Some Issue With Your Request. Please Try Again Later."));
        }
        return registeredProviders;
    }

    @Override
    public Map<String, Boolean> checkEmailExist(String email) {
        List<String> registeredemail = adminDao.checkEmailExist(email);
        Map<String,Boolean> emailExist = new HashMap<>();
        if(registeredemail.isEmpty())
        {
            emailExist.put("isEmailExist",false);
        }
        else
        {
            emailExist.put("isEmailExist",true);
        }
        return emailExist;
    }

    @Override
    public ServiceResult<String> updatePassword(AdminPassword adminPassword) {
        int updateStatus = adminDao.updatePassword(adminPassword);
        if(updateStatus >0)
        {
            return new ServiceResult<>(200,"Password Updated Successfully");
        }
        else if(updateStatus == -1)
        {
            return new ServiceResult<>(400,"Old Password does not match");
        }
        else {
            return new ServiceResult<>(500,"We Cannot Update Your Password Right Now. Please Try Again.");
        }
    }

    @Override
    public ServiceResult<String> updateRegisteredProviderStatus(int registrationId, int status) {
        int updateStatus = adminDao.updateStatus(registrationId,status);
        if(updateStatus >0)
        {
            return new ServiceResult<>(200,"Status Updated Successfully");
        }
        return new ServiceResult<>(500,"We Cannot Update Status Right Now. Please Try Again.");
    }

    @Override
    public ServiceResult<String> updateRegisterationDetails(UpdateRegistrationDetails updateDetails) {
        int updateStatus = adminDao.updateRegistrationDetails(updateDetails);
        if(updateStatus >0)
        {
            return new ServiceResult<>(200,"Updated Successfully");
        }
        return new ServiceResult<>(500,"We Cannot Update Right Now. Please Try Again.");
    }
}
