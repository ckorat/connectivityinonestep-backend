package com.connectivityinonestep.admin.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@Data
@AllArgsConstructor
public class AdminPassword implements Serializable {

    int adminId;
    String oldPassword;
    String newPassword;
}
