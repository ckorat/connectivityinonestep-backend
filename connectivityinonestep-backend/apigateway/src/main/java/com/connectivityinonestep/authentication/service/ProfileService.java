package com.connectivityinonestep.authentication.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.connectivityinonestep.authentication.model.ProfileModel;
import com.connectivityinonestep.authentication.repository.ProfileRepository;

@Service
@Qualifier("p")
public class ProfileService implements UserDetailsService {

	public static String tableName;

	@Autowired
	private ProfileRepository profileRepository;

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		final List<ProfileModel> users = profileRepository.getAllProfiles(tableName);
		System.out.println("users"+users);
		for (ProfileModel appUser : users) {
			if (appUser.getEmail().equals(email)) {
				System.out.println(appUser);
//				List<GrantedAuthority> grantedAuthorities =
				List<GrantedAuthority> grantedAuthorities = AuthorityUtils
						.commaSeparatedStringToAuthorityList("ROLE_" + "admin".toUpperCase());
				return new User(appUser.getEmail(), appUser.getPassword(), grantedAuthorities);
			}
		}
		throw new UsernameNotFoundException("Email: " + email + " not found");
	}

	public ProfileModel getProfileByEmail(String email) {
		return profileRepository.getProfileByEmail(email);
	}

}
