package com.connectivityinonestep.authentication.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Repository;

import com.connectivityinonestep.authentication.model.ProfileModel;

@Repository
public class ProfileRepository {

	private String tableName;

	@Autowired
	private BCryptPasswordEncoder encoder;

	@Autowired
	JdbcTemplate jdbcTemplate;

	public List<ProfileModel> getAllProfiles(String tableName) {
		this.tableName = tableName;
		return jdbcTemplate.query("select * from " + tableName, new RowMapper<ProfileModel>() {
			@Override
			public ProfileModel mapRow(ResultSet rs, int rowNum) throws SQLException {
				ProfileModel model = new ProfileModel();
				model.setId(rs.getInt("registrationId"));
				model.setEmail(rs.getString("email"));
				model.setPassword(encoder.encode(rs.getString("password")));
				return model;
			}
		});
	}

	public ProfileModel getProfileByEmail(String email) {
		return jdbcTemplate.queryForObject("select * from " + tableName + " where email='" + email + "'",
				new RowMapper<ProfileModel>() {
					@Override
					public ProfileModel mapRow(ResultSet rs, int rowNum) throws SQLException {
						ProfileModel model = new ProfileModel();
						model.setId(rs.getInt("registrationId"));
						model.setEmail(rs.getString("email"));
						model.setPassword(encoder.encode(rs.getString("password")));
						System.out.println("model"+model);
						return model;
					}
				});
	}
}
