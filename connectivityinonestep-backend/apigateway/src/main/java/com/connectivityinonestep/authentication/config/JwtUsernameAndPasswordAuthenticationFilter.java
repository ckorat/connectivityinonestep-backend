package com.connectivityinonestep.authentication.config;

import java.io.IOException;
import java.sql.Date;
import java.util.Collections;
import java.util.stream.Collectors;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.connectivityinonestep.authentication.model.LoginModel;
import com.connectivityinonestep.authentication.model.ProfileForLoginState;
import com.connectivityinonestep.authentication.model.ProfileModel;
import com.connectivityinonestep.authentication.service.ProfileService;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class JwtUsernameAndPasswordAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
	private AuthenticationManager authManager;
	private final JwtConfig jwtConfig;
	private ProfileService profileService;
	private ObjectMapper mapper = new ObjectMapper();

	public JwtUsernameAndPasswordAuthenticationFilter(AuthenticationManager authManager, JwtConfig jwtConfig,
			ProfileService profileService) {
		this.authManager = authManager;
		this.jwtConfig = jwtConfig;
		this.profileService = profileService;
		this.setRequiresAuthenticationRequestMatcher(new AntPathRequestMatcher(jwtConfig.getUri(), "POST"));
	}

	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
			throws AuthenticationException {
		try {
			LoginModel creds = new ObjectMapper().readValue(request.getInputStream(), LoginModel.class);
			ProfileService.tableName = creds.getEmail().split("<>")[1];
			creds.setEmail(creds.getEmail().split("<>")[0]);
			System.out.println(creds);
			UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(creds.getEmail(),
					creds.getPassword(), Collections.emptyList());
			System.out.println("authtoken"+authToken);
			return authManager.authenticate(authToken);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
			Authentication auth) throws IOException, ServletException {
		try{
			System.out.println(auth.getName());
			Long now = System.currentTimeMillis();
			String token = Jwts.builder().setSubject(auth.getName())
					.claim("authorities",
							auth.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()))
					.setIssuedAt(new Date(now)).setExpiration(new Date(now + jwtConfig.getExpiration() * 1000))
					.signWith(SignatureAlgorithm.HS512, jwtConfig.getSecret().getBytes()).compact();
			response.addHeader(jwtConfig.getHeader(), jwtConfig.getPrefix() + " " + token);
			response.setContentType("application/json");
			response.setCharacterEncoding("utf-8");

			ProfileModel profileModel = profileService.getProfileByEmail(auth.getName());
			System.out.println("profileModel"+profileModel);
			response.getWriter().write(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(new ProfileForLoginState(
					profileModel.getId(), profileModel.getEmail())));
		}catch (Exception e)
		{
			System.out.println(e);
		}

	}
}
