package com.connectivityinonestep.messagingservice.service;


import com.connectivityinonestep.messagingservice.Model.Mail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Service("mailService")
public class MailService implements IMailService {

    @Qualifier("getMailSender")
    @Autowired
    JavaMailSender javaMailSender;

    @Override
    public void sendEmail(Mail mail) throws MessagingException {
        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message,true);
        helper.setTo(mail.getTo());
        helper.setSubject(mail.getSubject());
        helper.setText(mail.getContent(),true);
        javaMailSender.send(message);
    }
}
