package com.connectivityinonestep.messagingservice.controller;

import com.connectivityinonestep.messagingservice.Model.Mail;
import com.connectivityinonestep.messagingservice.service.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;

@RestController
@RequestMapping(path="/messaging")
public class MessagingServiceController {

    @Autowired
    MailService emailService;

    @PostMapping("/sendmail")
    public void sendMail(@RequestBody Mail mail) throws MessagingException {
        emailService.sendEmail(mail);
    }
}
