package com.connectivityinonestep.messagingservice.service;

import com.connectivityinonestep.messagingservice.Model.Mail;

import javax.mail.MessagingException;

public interface IMailService {
    public void sendEmail(Mail mail) throws MessagingException;
}
