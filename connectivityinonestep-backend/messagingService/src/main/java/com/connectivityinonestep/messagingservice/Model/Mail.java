package com.connectivityinonestep.messagingservice.Model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Mail {

    private String to;
    private String Subject;
    private String Content;
    private String name;
}
