package com.connectivityinonestep.provider.service;

import com.connectivityinonestep.provider.model.Package;
import com.connectivityinonestep.provider.model.Provider;
import com.connectivityinonestep.provider.repository.IProviderDao;
import com.connectivityinonestep.provider.utils.ServiceProperties;
import exception.ResultException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import response.ServiceResult;
import java.util.List;
import java.util.Map;

@Service
public class ProviderService implements IProviderService {
    @Autowired
    private IProviderDao providerDao;

    @Autowired
    private ServiceProperties properties;

    @Override
    public List<Provider> getAllProviders() {
        List<Provider> providers = providerDao.getAllProviders();
        if(providers.isEmpty())
        {
            throw new ResultException(new ServiceResult<>(404,properties.getMessages().getNotFound()));
        }
        return providerDao.getAllProviders();
    }

    @Override
    public List<Provider> getProviderById(int providerId) {
        List<Provider> provider = providerDao.getProviderById(providerId);
        if(provider.isEmpty())
        {
            throw new ResultException(new ServiceResult<>(404,properties.getMessages().getNotFound()));
        }
        return providerDao.getProviderById(providerId);
    }

    @Override
    public Map<String,Object> getAllPackagesByPostalCode(int postalCode, int pageSize, int offset, Map<String,String> allRequestParameters) {
        providerDao.checkForValidPostalCode(postalCode);
        Map<String,Object> packages = providerDao.getAllPackagesByPostalCode(postalCode,pageSize,offset,allRequestParameters);
        if(packages.isEmpty())
        {
            throw new ResultException(new ServiceResult<>(500,properties.getMessages().getInternalServerError()));
        }
        return packages;
    }

    @Override
    public Map<String,Object> getContractTermsByPackageId(int packageId) {
        Map<String,Object> contractTerms = providerDao.getContractTermsByPackageId(packageId);
        if(contractTerms.get(0) == null)
        {
            throw new ResultException(new ServiceResult<>(404,"No Contract Terms Available for this Package"));
        }
        return contractTerms;
    }

    @Override
    public Map<String, Object> applyFilters(int postalCode){
        providerDao.checkForValidPostalCode(postalCode);
        return providerDao.applyFilters(postalCode);
    }

    @Override
    public int getPostalCoverageByProviderId(int providerId) {
        return providerDao.getProvidersCoverage(providerId);
    }

}
