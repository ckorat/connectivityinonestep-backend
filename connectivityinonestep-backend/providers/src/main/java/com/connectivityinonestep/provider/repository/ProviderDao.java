package com.connectivityinonestep.provider.repository;

import com.connectivityinonestep.provider.model.*;
import com.connectivityinonestep.provider.model.Package;
import com.connectivityinonestep.provider.model.mapper.*;
import com.connectivityinonestep.provider.utils.ServiceProperties;
import exception.ResultException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SingleColumnRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import response.ServiceResult;

import java.util.*;

@Repository
public class ProviderDao implements IProviderDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    private ServiceProperties properties;

    @Override
    public List<Provider> getAllProviders() {
        return jdbcTemplate.query(properties.getDbQueries().getAllProviders(),new ProviderRowMapper());
    }

    @Override
    public List<Provider> getProviderById(int providerId) {
        return jdbcTemplate.query(properties.getDbQueries().getProvidersById(),new ProviderRowMapper(),providerId);
    }

    @Override
    public Map<String,Object> getAllPackagesByPostalCode(int postalCode,int pageSize, int offset,Map<String,String> allRequestParameters) {
        Map<String,Object> allPackages = new HashMap<>();
        List<Integer> providerIdList = jdbcTemplate.query(properties.getDbQueries().getProvidersByPostalCode(),new SingleColumnRowMapper<>(),postalCode);
        if(providerIdList.isEmpty())
        {
            throw new ResultException(new ServiceResult<>(404,"No Providers Available at this Postal Code"));
        }
        String providerIds = StringUtils.join(providerIdList,",");
        String sql=null;

        if(allRequestParameters.isEmpty())
        {
            sql = "select p.providerId,p.logoUrl,p.providerName,pa.* from providers p, packages pa where p.providerId=pa.providerId and pa.providerId IN ("+providerIds+") order by pa.price Limit "+pageSize+" offset "+offset;
        }
        else
        {
            String downloadSpeed=allRequestParameters.get("downloadSpeed").replaceAll(",$","");
            String price=allRequestParameters.get("price").replaceAll(",$","");
            String priceArray[] = price.split(",");
            String providerNames=allRequestParameters.get("providerName").replaceAll(",$","");
            String providerNamesArray[] = providerNames.split(",");
            String updatedProviderNames = "'" + StringUtils.join(providerNamesArray,"','") + "'";
            String sortBy=allRequestParameters.get("sortby").replaceAll(",$","");
            sql = "select p.providerId,p.logoUrl,p.providerName,pa.* from providers p, packages pa where p.providerId=pa.providerId and pa.providerId IN ("+providerIds+") and pa.price>="+priceArray[0]+" and pa.price<="+priceArray[1]+"";
            if(!downloadSpeed.isEmpty())
            {
                sql += " and pa.downloadSpeed IN ("+downloadSpeed+")";
            }
            if(!updatedProviderNames.contains("''"))
            {
                sql+=" and p.providerName IN ("+updatedProviderNames+")";
            }
            if(!sortBy.isEmpty())
            {
                sql+= " order by pa.price "+sortBy;
            }
            sql+=" Limit "+pageSize+" offset "+offset;
        }
        List<Package> packageList = jdbcTemplate.query(sql,new PacakageRowMapper());
        String totalPackage = "select DISTINCT count(pa.packageId) as totalPackages from packages pa,providers p where p.providerId IN ("+providerIds+") GROUP by p.providerId";
        Integer total = jdbcTemplate.queryForObject(totalPackage,Integer.class);
        allPackages.put("totalCount",total);
        allPackages.put("packages",packageList);
        return allPackages;

    }


    @Override
    public Map<String,Object> getContractTermsByPackageId(int packageId) {
        Map<String,Object> contractTermMap = new HashMap<>();
        List<Map<String,Object>> offersAndDuration= new ArrayList<>();

        String durationIds = properties.getDbQueries().getUniqueDurationIds();
        List<Integer> durationIdList = jdbcTemplate.query(durationIds,new SingleColumnRowMapper<>(),packageId);

        for(int durationId: durationIdList)
        {
            Map<String,Object> temp_map = new HashMap<>();
            temp_map.put("duration", jdbcTemplate.queryForObject(properties.getDbQueries().getDurationValue(),new Object[]{durationId},String.class));
            temp_map.put("offers",jdbcTemplate.query(properties.getDbQueries().getOffersIncludedInDuration(),new SingleColumnRowMapper<>(),durationId));
            offersAndDuration.add(temp_map);
        }
        contractTermMap.put("ContractTerms",offersAndDuration);
        return contractTermMap;
    }

    @Override
    public Map<String,Object> applyFilters(int postalCode) {

            List<Integer> providerIdList = jdbcTemplate.query(properties.getDbQueries().getProvidersByPostalCode(),new SingleColumnRowMapper<>(),postalCode);
            String providerIds = StringUtils.join(providerIdList,",");
            if(providerIds.isEmpty())
            {
                throw new ResultException(new ServiceResult<>(404,"No Providers Available at this Postal Code"));
            }
            String downloadSpeedSql = "select DISTINCT pa.downloadSpeed from providers p, packages pa where p.providerId=pa.providerId and pa.providerId IN ("+providerIds+") order by downloadSpeed";
            String minPriceSql = "select min(pa.price) from providers p, packages pa where p.providerId=pa.providerId and pa.providerId IN ("+providerIds+")";
            String maxPriceSql="select max(pa.price) from providers p, packages pa where p.providerId=pa.providerId and pa.providerId IN ("+providerIds+")";
            String providerNamesSql = "select DISTINCT p.providerName from providers p, packages pa where p.providerId=pa.providerId and pa.providerId IN ("+providerIds+") order by providerName";

            List<Integer> downloadSpeed = jdbcTemplate.query(downloadSpeedSql, new SingleColumnRowMapper<>());
            Integer minPrice = jdbcTemplate.queryForObject(minPriceSql,Integer.class);
            Integer maxPrice = jdbcTemplate.queryForObject(maxPriceSql,Integer.class);
            List<String> providerName = jdbcTemplate.query(providerNamesSql,new SingleColumnRowMapper<>());

            Map<String,Object> filters = new HashMap<>();
            Map<String,Integer> priceObject = new HashMap<>();
            priceObject.put("min",minPrice);
            priceObject.put("max",maxPrice);

            filters.put("downloadSpeed",downloadSpeed);
            filters.put("price",priceObject);
            filters.put("providerName",providerName);
            return filters;

    }

    @Override
    public void checkForValidPostalCode(int postalCode) {
        List<String> postalCodeList = jdbcTemplate.query(properties.getDbQueries().getValidPostalCode(),new SingleColumnRowMapper<>(),postalCode);
        if(postalCodeList.isEmpty()) {
            throw new ResultException(new ServiceResult<>(400, "This Postal Code is not available!!"));
        }
    }

    @Override
    public int getProvidersCoverage(int providerId) {
        int totalServiceableAreas = jdbcTemplate.queryForObject(properties.getDbQueries().getTotalServiceableArea(),Integer.class);
        System.out.println(totalServiceableAreas);
        int totalProvidersServiceable = jdbcTemplate.queryForObject(properties.getDbQueries().getTotalServicableAreasOfProviders(),new Object[]{providerId},Integer.class);
        System.out.println(totalProvidersServiceable);
        int postalCoverage = (totalProvidersServiceable * 100)/totalServiceableAreas;
        System.out.println(postalCoverage);
        return postalCoverage;
    }


}
