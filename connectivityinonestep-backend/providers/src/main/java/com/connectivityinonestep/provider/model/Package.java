package com.connectivityinonestep.provider.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Package implements Serializable {

    private int packageId;
    private int downloadSpeed;
    private int uploadSpeed;
    private String suffix;
    private String dataCapped;
    private String dataType;
    private String price;
    private int providerid;
    private String logoUrl;
    private String providerName;
}
