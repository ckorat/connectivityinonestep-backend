package com.connectivityinonestep.provider.repository;

import com.connectivityinonestep.provider.model.Provider;

import java.util.List;
import java.util.Map;

public interface IProviderDao {
    List<Provider> getAllProviders();
    List<Provider> getProviderById(int providerId);
    Map<String,Object> getAllPackagesByPostalCode(int postalCode, int pageSize, int offset,Map<String,String> allRequestParameters);
    Map<String,Object> getContractTermsByPackageId(int packageId);
    Map<String,Object>  applyFilters(int postalCode);
    void checkForValidPostalCode(int postalCode);
    int getProvidersCoverage(int providerId);

}
