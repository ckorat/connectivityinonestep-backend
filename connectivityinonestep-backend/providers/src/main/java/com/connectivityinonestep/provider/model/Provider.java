package com.connectivityinonestep.provider.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Provider implements Serializable {

    private int providerId;
    private String providerName;
    private String description;
    private String contactNumber;
    private String logoUrl;
    private String address;
    private int maxSpeed;



}
