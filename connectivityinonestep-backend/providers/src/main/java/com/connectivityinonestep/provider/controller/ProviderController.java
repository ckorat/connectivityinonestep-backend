package com.connectivityinonestep.provider.controller;

import com.connectivityinonestep.provider.model.Package;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;
import response.ServiceResult;
import com.connectivityinonestep.provider.exception.ProviderException;
import com.connectivityinonestep.provider.model.Provider;
import com.connectivityinonestep.provider.service.IProviderService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/providers")
public class ProviderController {
    @Autowired
    private IProviderService providerService;

    @ApiOperation(value = "get All Providers",notes="Get all Providers")
    @ApiResponses(value = {
            @ApiResponse(code=200,message = "Successful",response = Provider.class),
            @ApiResponse(code=500,message = "INTERNAL SERVER ERROR",response = ProviderException.class)
    })
    @GetMapping("/")
    public ResponseEntity<ServiceResult<List<Provider>>> getAllProviders()
    {
        ServiceResult result = new ServiceResult<>(200,providerService.getAllProviders());
        return new ResponseEntity<>(result, HttpStatus.valueOf(result.getCode()));
    }


    @GetMapping("/{providerId}")
    public ResponseEntity<ServiceResult<List<Provider>>> getProviderById(@PathVariable("providerId") int providerId)
    {
        ServiceResult result = new ServiceResult<>(200,providerService.getProviderById(providerId));
        return new ResponseEntity<>(result, HttpStatus.valueOf(result.getCode()));
    }


    @GetMapping("/PostalCoverage/{providerId}")
    public ResponseEntity<ServiceResult<Integer>> getProvidersCoverage(@PathVariable("providerId") int providerId)
    {
        ServiceResult result = new ServiceResult<>(200,providerService.getPostalCoverageByProviderId(providerId));
        return new ResponseEntity<>(result, HttpStatus.valueOf(result.getCode()));
    }


    @ApiOperation(value = "Get All Packages by provider id",notes="Get all Packages By ProviderId")
    @ApiResponses(value = {
            @ApiResponse(code=200,message = "Successful",response = Package.class),
            @ApiResponse(code=400,message = "Bad Request",response = ProviderException.class),
            @ApiResponse(code=404,message = "Not Found",response = ProviderException.class),
            @ApiResponse(code=500,message = "INTERNAL SERVER ERROR",response = ProviderException.class)
    })
    @GetMapping("/packages/{postalCode}/{pageSize}/{offset}")
    public ResponseEntity<ServiceResult<Map<String,Object>>> getPackagesByPostalCode(
            @ApiParam @PathVariable("postalCode") int postalCode,
            @ApiParam @PathVariable("pageSize") int pageSize,
            @ApiParam @PathVariable("offset") int offset,
            @ApiParam @RequestParam(required = false) Map<String,String> allRequestParameters
    )
    {
        ServiceResult result = new ServiceResult<>(200,providerService.getAllPackagesByPostalCode(postalCode,pageSize,offset,allRequestParameters));
        return new ResponseEntity<>(result, HttpStatus.valueOf(result.getCode()));
    }


    @ApiOperation(value = "Get Contract Terms by Package Id",notes="Get all contract terms by package id")
    @ApiResponses(value = {
            @ApiResponse(code=200,message = "Successful",response = Package.class),
            @ApiResponse(code=400,message = "BAD REQUEST",response = ProviderException.class),
            @ApiResponse(code=404,message = "NOT FOUND",response = ProviderException.class),
            @ApiResponse(code=500,message = "INTERNAL SERVER ERROR",response = ProviderException.class)
    })
    @GetMapping("/packages/{packageId}/contractTerms")
    public ResponseEntity<ServiceResult<Map<String,Object>>> getContractTermsByPackageId(
            @ApiParam @PathVariable("packageId") int packageId
    )
    {
        ServiceResult result = new ServiceResult<>(200,providerService.getContractTermsByPackageId(packageId));
        return new ResponseEntity<>(result, HttpStatus.valueOf(result.getCode()));
    }


    @GetMapping("/{postalCode}/applyfilters")
    public ResponseEntity<ServiceResult<Map<String,Object>>> applyFilters(@PathVariable("postalCode") int postalCode)
    {
        ServiceResult result = new ServiceResult<>(200,providerService.applyFilters(postalCode));
        return new ResponseEntity<>(result, HttpStatus.valueOf(result.getCode()));
    }

}
