package com.connectivityinonestep.provider.model.mapper;

import com.connectivityinonestep.provider.model.Package;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PacakageRowMapper implements RowMapper<Package> {
    @Override
    public Package mapRow(ResultSet row, int rowNum) throws SQLException {
        Package packageDetails = new Package();
        packageDetails.setPackageId(row.getInt("packageId"));
        packageDetails.setProviderid(row.getInt("providerId"));
        packageDetails.setDownloadSpeed(row.getInt("downloadSpeed"));
        packageDetails.setUploadSpeed(row.getInt("uploadSpeed"));
        packageDetails.setDataCapped(row.getString("dataCapped"));
        packageDetails.setDataType(row.getString("dataType"));
        packageDetails.setPrice(row.getString("price"));
        packageDetails.setLogoUrl(row.getString("logoUrl"));
        packageDetails.setSuffix(row.getString("suffix"));
        packageDetails.setProviderName(row.getString("providerName"));
        return packageDetails;
    }
}
