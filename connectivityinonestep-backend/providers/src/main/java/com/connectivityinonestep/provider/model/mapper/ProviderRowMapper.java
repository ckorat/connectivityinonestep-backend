package com.connectivityinonestep.provider.model.mapper;

import com.connectivityinonestep.provider.model.Provider;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ProviderRowMapper implements RowMapper<Provider> {
    @Override
    public Provider mapRow(ResultSet row, int rowNum) throws SQLException {
        Provider provider = new Provider();
        provider.setProviderId(row.getInt("providerId"));
        provider.setProviderName(row.getString("providerName"));
        provider.setAddress(row.getString("address"));
        provider.setContactNumber(row.getString("contactNumber"));
        provider.setDescription(row.getString("description"));
        provider.setLogoUrl(row.getString("logoUrl"));
        provider.setMaxSpeed(row.getInt("maxSpeed"));
        return provider;
    }
}
