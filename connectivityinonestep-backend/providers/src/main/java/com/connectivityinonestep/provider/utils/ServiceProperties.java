package com.connectivityinonestep.provider.utils;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ConfigurationProperties(prefix = "db")
@Component
public class ServiceProperties {

    @NotNull
    public DbQueries dbQueries;

    @Data
    @Component
    @AllArgsConstructor
    @NoArgsConstructor
    public static class DbQueries {
        private String allProviders;
        private String providersById;
        private String AllPackagesByProviderId;
        private String getContractTerm;
        private String contractTermByPackageId;
        private String allOffers;
        private String providersByPostalCode;
        private String validPostalCode;
        private String uniqueDurationIds;
        private String offersIncludedInDuration;
        private String durationValue;
        private String totalServiceableArea;
        private String totalServicableAreasOfProviders;

    }

    @NotNull
    public Messages messages;

    @Data
    @Component
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Messages{
        private String notFound;
        private String internalServerError;
    }

}
